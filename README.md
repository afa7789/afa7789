# 👋🕶️ [About me](https://github.com/afa7789) , check my github

🐍 Arthur de Freitas Abeilice, the last of my name. </br>

♍ 1996.</br>

💻 Computer Engineer bachelor's - [CEFET-MG Federal Center of Technoligical Education of Minas Gerais](https://www.cefetmg.br).</br>

🏠 I’m currently living at Belo Horizonte - MG , Brazil 🇧🇷. <br/>

👨‍💻 I’m a Software Engineer , Full-Stack Developer.<br/>

### 🌐 Social Links:

Wanna Talk? Hit me up here:

[![Github Badge](https://img.shields.io/badge/-afa7789-grey?style=flat-square&logo=Github&logoColor=white&link=https://github.com/afa7789)](https://github.com/afa7789)

</br>

![Visitor Badge](https://visitor-badge.laobi.icu/badge?page_id=afa7789.afa7789)
